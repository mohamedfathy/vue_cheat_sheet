var vm = new Vue({
  el:'#app',
  data: {
    firstName: 'Mohamed',
    lastName: 'Fathy'
 },
 computed: {
   fullName:{
    // getter
    get: function () {
      return this.firstName + ' ' + this.lastName;
    },
    set: function (val) {
      var Names = val.split(' ');
      this.firstName = Names[0];
      this.lastName = Names[Names.length - 1];
    }
   } 
 },
  mounted:  function () {
    console.log(this.firstName);
 }
});

vm.fullName = "Ahmed Hassan";