var vm = new Vue({
  el:'#app',
  data: {
    firstName: 'Mohamed',
    lastName: 'Fathy',
    fullName: 'Mohamed Fathy'
 },
 watch: {
   firstName: function (val) {
     this.fullName = val + ' ' + this.lastName;
   },
   lastName: function (val) {
     this.fullName = this.firstName + ' ' + val;
   }
 },
 computed: {
   computedFullName: function () {
     return this.firstName + ' ' + this.lastName;
   }
 }
});

vm.firstName = "Osama";