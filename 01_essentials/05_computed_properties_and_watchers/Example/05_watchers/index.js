var vm = new Vue({
  el:'#app',
  data: {
    question: '',
    answer: 'I cant give you answer unless you ask.'
  },
  watch: {
    question: function (newQuestion, oldQuestion) {
      this.answer = "waiting for you to stop typing.";
      this.debouncedGetAnswer();
    }
  },
  created: function () {
    this.debouncedGetAnswer = _.debounce(this.getAnswer, 500);
  },
  methods: {
    getAnswer: function (){
      if (this.question.indexOf('?') === -1 ) {
        this.answer = 'Questions usually contians an question mark.'
        return;
      }
      this.answer = 'Thinking...';
      axios.get('https://yesno.wtf/api')
        .then(function (response) {
          vm.answer = _.capitalize(response.data.answer);
        })
        .catch(function (error) {
          vm.answer = 'Error! Could not reach API' + error;  
        })

    }
  }
 
});
