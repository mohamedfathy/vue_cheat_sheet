# Components Basics

### Base Example
- Components are reusable Vue instances with a name.
- Since components are reusable Vue instances, they accept the same options as new Vue, such as data, computed, watch, methods, and lifecycle hooks. The only exceptions are a few root-specific options like el.

```
// Define a new component called button-counter
Vue.component('button-counter', {
  data: function () {
    return {
      count: 0
    }
  },
  template: '<button v-on:click="count++">You clicked me {{ count }} times.</button>'
})

// we can you it here.
<button-counter></button-counter>

```

### Reusing Components
- each time you use a component, a new instance of it is created.

#### data Must Be a Function
- a component’s data option must be a function, so that each instance can maintain an independent copy of the returned data object:

```
data: function () {
  return {
    count: 0
  }
}
```

### Passing Data to Child Components with Props
- Props are custom attributes you can register on a component. 

```
Vue.component('blog-post', {
  props: ['title'],
  template: '<h3>{{ title }}</h3>'
})

// Once a prop is registered, you can pass data to it as a custom attribute.
<blog-post title="My journey with Vue"></blog-post>
```

```
new Vue({
  el: '#blog-post-demo',
  data: {
    posts: [
      { id: 1, title: 'My journey with Vue' },
      { id: 2, title: 'Blogging with Vue' },
      { id: 3, title: 'Why Vue is so fun' }
    ]
  }
})

<blog-post
  v-for="post in posts"
  v-bind:key="post.id"
  v-bind:title="post.title"
></blog-post>
```

### A Single Root Element
- every component must have a single root element. 

```
// this code will not work 
<h3>{{ title }}</h3>
<div v-html="content"></div>

// this will work.
<div class="blog-post">
  <h3>{{ title }}</h3>
  <div v-html="content"></div>
</div>
```

```
<blog-post
  v-for="post in posts"
  v-bind:key="post.id"
  v-bind:post="post"
></blog-post>
Vue.component('blog-post', {
  props: ['post'],
  template: `
    <div class="blog-post">
      <h3>{{ post.title }}</h3>
      <div v-html="post.content"></div>
    </div>
  `
})
```

### Listening to Child Components Events
- some features may require communicating back up to the parent.

```
Vue.component('blog-post', {
  props: ['post'],
  template: `
    <div class="blog-post">
      <h3>{{ post.title }}</h3>
      <button v-on:click="$emit('enlarge-text')">
        Enlarge text
      </button>
      <div v-html="post.content"></div>
    </div>
  `
})

new Vue({
  el: '#blog-posts-events-demo',
  data: {
    posts: [/* ... */],
    postFontSize: 1
  }
})

<div id="blog-posts-events-demo">
  <div :style="{ fontSize: postFontSize + 'em' }">
    <blog-post
      v-for="post in posts"
      v-bind:key="post.id"
      v-bind:post="post"
      v-on:enlarge-text="postFontSize += 0.1"
    ></blog-post>
  </div>
</div>

```

#### Emitting a Value With an Event
```
Vue.component('blog-post', {
  props: ['post'],
  template: `
    <div class="blog-post">
      <h3>{{post.title}}</h3>
      <p>{{post.description}}</p>
      <button v-on:click="$emit('enlarge-text', 0.1)">Enlarge Text</button>
    </div>
  `
});

let vm = new Vue({
  el: '#app',
  data: {
    posts: [
      {id:1, title: 'Listen to Music.', description: '...'},
      {id:2, title: 'Watch TV.', description: '...'},
      {id:3, title: 'Learn Vue.', description: '...'}
    ],
    postFontSize:1
  }
})

<div v-bind:style="{fontSize: postFontSize + 'em'}">
    <blog-post v-for="post in posts" v-bind:key="post.id" v-bind:post="post" v-on:enlarge-text="postFontSize += $event"></blog-post>
</div>
```

#### Using v-model on Components
# Not Understood.

### Content Distribution with Slots
- it’s often useful to be able to pass content to a component in the middle.

```
<alert-box>
  Something bad happened.
</alert-box>

Vue.component('alert-box', {
  template: `
    <div class="demo-alert-box">
      <strong>Error!</strong>
      <slot></slot>
    </div>
  `
})
```

### Dynamic Components
- currentTabComponent can contain either:
- the name of a registered component
- component’s options object

```
<!-- Component changes when currentTabComponent changes -->
<component v-bind:is="currentTabComponent"></component>
```

### DOM Template Parsing Caveats
- Some HTML elements, such as <ul>, <ol>, <table> and <select> have restrictions on what elements can appear inside them
- the is special attribute offers a workaround:

```
// this will cause error.
<table>
  <blog-post-row></blog-post-row>
</table>

// this will work.
<table>
  <tr is="blog-post-row"></tr>
</table>
```

#### It should be noted that this limitation does not apply if you are using string templates from one of the following sources:

- String templates (e.g. template: '...')
- Single-file (.vue) components
- <script type="text/x-template">