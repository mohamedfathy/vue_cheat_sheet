Vue.component('blog-post', {
  props: ['post'],
  template: `
    <div class="blog-post">
      <h3>{{post.title}}</h3>
      <p>{{post.description}}</p>
      <button v-on:click="$emit('enlarge-text', 0.1)">Enlarge Text</button>
    </div>
  `
});

let vm = new Vue({
  el: '#app',
  data: {
    posts: [
      {id:1, title: 'Listen to Music.', description: '...'},
      {id:2, title: 'Watch TV.', description: '...'},
      {id:3, title: 'Learn Vue.', description: '...'}
    ],
    postFontSize:1
  }
})