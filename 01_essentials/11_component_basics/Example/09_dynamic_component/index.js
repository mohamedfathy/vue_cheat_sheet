Vue.component('tab-home', {
  template: "<div>Home Component</div>"
});

Vue.component('tab-posts', {
  template: "<div>Post Component</div>"
});

Vue.component('tab-archive', {
  template: "<div>Archive Component</div>"
});

let vm = new Vue({
  el:'#app',
  data: {
    currentTab: 'Home',
    tabs: ['Home', 'Posts', 'Archive']
  },
  computed: {
    currentTabComponent: function () {
      return 'tab-' + this.currentTab.toLowerCase();
    }
  }
});