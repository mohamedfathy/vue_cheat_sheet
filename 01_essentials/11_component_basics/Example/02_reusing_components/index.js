Vue.component('button-counter', {
  data: function () {
    return {
      counter: 0
    }
  },
  template: "<button v-on:click=\"counter++\">You clicked me {{counter}} times.</button>"
});

let vm = new Vue({
  el: '#app'
});