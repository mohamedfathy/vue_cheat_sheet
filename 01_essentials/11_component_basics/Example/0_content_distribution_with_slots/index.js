Vue.component('alert-box',{
  template: `
    <div class="demo-alert-box">
      <strong>Error!</strong>
      <slot></slot>
    </div>
  `
})

var vm = new Vue({
  el:"#app",
  data: {
    posts: [
      {id:1, title: 'my journey with vue.', paragraph: 'this is the content of paragraph.',published_at: '2009-12-29'},
      {id:2, title: 'blogging with vue.', paragraph: 'this is the content of paragraph.',published_at: '2009-12-29'},
      {id:3, title: 'why vue is fun.', paragraph: 'this is the content of paragraph.',published_at: '2009-12-29'}
    ],
    postFontSize: 1
  }
});