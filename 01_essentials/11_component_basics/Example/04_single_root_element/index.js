Vue.component('my-post-one',{
  props: ['post'],
  template: `
    <h2>{{post.title}}</h2>

  `
})
Vue.component('my-post-two',{
  props: ['post'],
  template: `
    <div class="blog-post">
      <h2>{{post.title}}</h2>
      <p>{{post.paragraph}}</p>
      <p>{{post.published_at}}</p>
    </div>
  `
})
var vm = new Vue({
  el:"#app",
  data: {
    posts: [
      {id:1, title: 'my journey with vue.', paragraph: 'this is the content of paragraph.',published_at: '2009-12-29'},
      {id:2, title: 'blogging with vue.', paragraph: 'this is the content of paragraph.',published_at: '2009-12-29'},
      {id:3, title: 'why vue is fun.', paragraph: 'this is the content of paragraph.',published_at: '2009-12-29'}
    ]
  }
});