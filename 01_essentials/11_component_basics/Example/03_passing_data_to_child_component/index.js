Vue.component('blog-post', {
  props: ['title'],
  template : "<p>{{title}}</p>"
});

let vm = new Vue({
  el:'#app',
  data: {
    posts: [
      {id:1, title: 'Listen to Music.'},
      {id:2, title: 'Watch TV.'},
      {id:3, title: 'Learn Vue.'}
    ]
  }
});