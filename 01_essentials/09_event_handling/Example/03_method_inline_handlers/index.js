var vm = new Vue({
  el:"#app",
  methods: {
    say: function (message) {
      alert(message);
    },
    warn: function (message, event) {
      if (event) {
        event.preventDefault();
        console.log(event);
        alert(message);
      }
    }
  }
});