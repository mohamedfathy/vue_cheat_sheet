var vm = new Vue({
  el:"#app",
  data:{
    name: 'vue'
  },
  methods: {
    greet: function (event) {
      alert('Hello ' + this.name + ' !');
      if (event) {
        console.log('event', event.target);
      }
    }
  }
});