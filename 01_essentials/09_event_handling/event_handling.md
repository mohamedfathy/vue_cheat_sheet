# Event Handling

### Listening to events

- use the v-on directive to listen to DOM events and run some JavaScript when they’re triggered.

```
<div id="example-1">
  <button v-on:click="counter += 1">Add 1</button>
  <p>The button above has been clicked {{ counter }} times.</p>
</div>
var example1 = new Vue({
  el: '#example-1',
  data: {
    counter: 0
  }
})
```

### Method Event Handlers
- Event Handlers is function or method containing program statements that are executed in response to an event. An event handler typically is a software routine that processes actions such as keystrokes and mouse movements.  

```
<div id="example-2">
  <!-- `greet` is the name of a method defined below -->
  <button v-on:click="greet">Greet</button>
</div>
var example2 = new Vue({
  el: '#example-2',
  data: {
    name: 'Vue.js'
  },
  // define methods under the `methods` object
  methods: {
    greet: function (event) {
      // `this` inside methods points to the Vue instance
      alert('Hello ' + this.name + '!')
      // `event` is the native DOM event
      if (event) {
        alert(event.target.tagName)
      }
    }
  }
})
```

### Methods in Inline Handlers
- Instead of binding directly to a method name, we can also use methods in an inline JavaScript statement.

```
<div id="example-3">
  <button v-on:click="say('hi')">Say hi</button>
  <button v-on:click="say('what')">Say what</button>
</div>
new Vue({
  el: '#example-3',
  methods: {
    say: function (message) {
      alert(message)
    }
  }
})
```

- to access the original DOM event in an inline statement handler. You can pass it into a method using the special $event variable:

```
<button v-on:click="warn('Form cannot be submitted yet.', $event)">
  Submit
</button>
// ...
methods: {
  warn: function (message, event) {
    // now we have access to the native event
    if (event) event.preventDefault()
    alert(message)
  }
}
```

### Event Modifiers
- Vue provides event modifiers for v-on. Recall that modifiers are directive postfixes denoted by a dot.
- Order matters when using modifiers because the relevant code is generated in the same order. Therefore using v-on:click.prevent.self will prevent all clicks while v-on:click.self.prevent will only prevent clicks on the element itself.

```
.stop
.prevent
.capture
.self
.once
.passive

<!-- the click event's propagation will be stopped -->
<a v-on:click.stop="doThis"></a>

<!-- the submit event will no longer reload the page -->
<form v-on:submit.prevent="onSubmit"></form>

<!-- modifiers can be chained -->
<a v-on:click.stop.prevent="doThat"></a>

<!-- use capture mode when adding the event listener -->
<!-- i.e. an event targeting an inner element is handled here before being handled by that element -->
<div v-on:click.capture="doThis">...</div>

<!-- only trigger handler if event.target is the element itself -->
<div v-on:click.self="doThat">...</div>

<!-- the click event will be triggered at most once -->
<a v-on:click.once="doThis"></a>

<div v-on:scroll.passive="onScroll">...</div>
The .passive modifier is especially useful for improving performance on mobile devices.
```

### Key Modifiers
- Vue provides aliases for the most commonly used key codes when necessary for legacy browser support:

```
<input v-on:keyup.enter="submit">
<input v-on:keyup.page-down="onPageDown">

```
### Key Codes
- Vue provides aliases for the most commonly used key codes when necessary for legacy browser support:

```
.enter
.tab
.delete (captures both “Delete” and “Backspace” keys)
.esc
.space
.up
.down
.left
.right
```

- You can also define custom key modifier aliases via the global config.keyCodes object:
```
Vue.config.keyCodes.f1 = 112
```

### System Modifier Keys
- You can use the following modifiers to trigger mouse or keyboard event listeners only when the corresponding modifier key is pressed:

```
.ctrl
.alt
.shift
.meta

<!-- Alt + C -->
<input @keyup.alt.67="clear">

<!-- Ctrl + Click -->
<div @click.ctrl="doSomething">Do something</div>
```

#### .exact Modifier
- The .exact modifier allows control of the exact combination of system modifiers needed to trigger an event.

```
<!-- this will fire even if Alt or Shift is also pressed -->
<button @click.ctrl="onClick">A</button>
```

#### Mouse Button Modifiers

```
.left
.right
.middle
```