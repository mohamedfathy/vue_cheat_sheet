# List Rendering

## v-for 

### Mapping an Array with v-for

```
<ul id="example-1">
  <li v-for="item in items">
    {{ item.message }}
  </li>
</ul>
var example1 = new Vue({
  el: '#example-1',
  data: {
    items: [
      { message: 'Foo' },
      { message: 'Bar' }
    ]
  }
})
```

- Inside v-for blocks we have full access to parent scope properties.
- You can also use "of" as the delimiter instead of "in"

```
<ul id="example-2">
  <li v-for="(item, index) of items">
    {{ parentMessage }} - {{ index }} - {{ item.message }}
  </li>
</ul>
var example2 = new Vue({
  el: '#example-2',
  data: {
    parentMessage: 'Parent',
    items: [
      { message: 'Foo' },
      { message: 'Bar' }
    ]
  }
})
```

### v-for with an Object
- You can also use v-for to iterate through the properties of an object.
- When iterating over an object, the order is based on the enumeration order of Object.keys(), which is not guaranteed to be consistent across JavaScript engine implementations.

```
<ul id="v-for-object" class="demo">
  // getting only the value.
  <li v-for="value in object">
  // getting value and name.
  <div v-for="(value, name) in object">
  // getting value, name and index
  <div v-for="(value, name, index) in object">
    {{ index }}. {{ name }}: {{ value }}
  </li>
</ul>
new Vue({
  el: '#v-for-object',
  data: {
    object: {
      title: 'How to do lists in Vue',
      author: 'Jane Doe',
      publishedAt: '2016-04-10'
    }
  }
})
```

### Maintaining State
- to give Vue a hint so that it can track each node’s identity, you need to provide a unique key attribute for each item.
```
<div v-for="item in items" v-bind:key="item.id">
  <!-- content -->
</div>
```

### Array Change Detection

#### Mutation Methods
- Mutation methods, as the name suggests, mutate (change) the original array they are called on.
```
push()
pop()
shift()
unshift()
splice()
sort()
reverse()
```

### Replacing an Array
- there are also non-mutating methods, e.g. filter(), concat() and slice(), which do not mutate the original array but always return a new array.

```
example1.items = example1.items.filter(function (item) {
  return item.message.match(/Foo/)
})
```

### Caveats
- Due to limitations in JavaScript, Vue cannot detect the following changes to an array:

- When you directly set an item with the index, e.g. vm.items[indexOfItem] = newValue
- When you modify the length of the array, e.g. vm.items.length = newLength

```
var vm = new Vue({
  data: {
    items: ['a', 'b', 'c']
  }
})
vm.items[1] = 'x' // is NOT reactive
vm.items.length = 2 // is NOT reactive
```

```
// To overcome caveat 1
Vue.set(vm.items, indexOfItem, newValue)
// Array.prototype.splice
vm.items.splice(indexOfItem, 1, newValue)

// To deal with caveat 2, you can use splice:
vm.items.splice(newLength)
```

### Object Change Detection Caveats
#### property addition or deletion 
- Vue cannot detect property addition or deletion.

```
var vm = new Vue({
  data: {
    a: 1
  }
})
// `vm.a` is now reactive

vm.b = 2
// `vm.b` is NOT reactive
```

- Vue does not allow dynamically adding new root-level reactive properties to an already created instance. However, it’s possible to add reactive properties to a nested object using the Vue.set(object, propertyName, value) method.

```
var vm = new Vue({
  data: {
    userProfile: {
      name: 'Anika'
    }
  }
})

Vue.set(vm.userProfile, 'age', 27)

```

- assign a number of new properties to an existing object 
```
Object.assign(vm.userProfile, {
  age: 27,
  favoriteColor: 'Vue Green'
})
```

### Displaying Filtered/Sorted Results
- Sometimes we want to display a filtered or sorted version of an array without actually mutating or resetting the original data. In this case, you can create a computed property that returns the filtered or sorted array.

```
<li v-for="n in evenNumbers">{{ n }}</li>
data: {
  numbers: [ 1, 2, 3, 4, 5 ]
},
computed: {
  evenNumbers: function () {
    return this.numbers.filter(function (number) {
      return number % 2 === 0
    })
  }
}
```

- In situations where computed properties are not feasible (e.g. inside nested v-for loops), you can use a method:

```
<li v-for="n in even(numbers)">{{ n }}</li>
data: {
  numbers: [ 1, 2, 3, 4, 5 ]
},
methods: {
  even: function (numbers) {
    return numbers.filter(function (number) {
      return number % 2 === 0
    })
  }
}
```

### v-for with a Range
- v-for can also take an integer.

```
<div>
  <span v-for="n in 10">{{ n }} </span>
</div>
```


### v-for on a <template>
- Similar to template v-if, you can also use a <template> tag with v-for to render a block of multiple elements.

<ul>
  <template v-for="item in items">
    <li>{{ item.msg }}</li>
  </template>
</ul>

### v-for with v-if
- When they exist on the same node, v-for has a higher priority than v-if. That means the v-if will be run on each iteration of the loop separately.

```
<li v-for="todo in todos" v-if="!todo.isComplete">
  {{ todo }}
</li>
```

- If instead, your intent is to conditionally skip execution of the loop, you can place the v-if on a wrapper element.

```
<ul v-if="todos.length">
  <li v-for="todo in todos">
    {{ todo }}
  </li>
</ul>
```

### v-for with a Component
- You can directly use v-for on a custom component.

```
<my-component v-for="item in items" :key="item.id"></my-component>
```

- The reason for not automatically injecting item into the component is because that makes the component tightly coupled to how v-for works. 
- Being explicit about where its data comes from makes the component reusable in other situations.

```
<my-component
  v-for="(item, index) in items"
  v-bind:item="item"
  v-bind:index="index"
  v-bind:key="item.id"
></my-component>
```

Here’s a complete example of a simple todo list:

<div id="todo-list-example">
  <form v-on:submit.prevent="addNewTodo">
    <label for="new-todo">Add a todo</label>
    <input
      v-model="newTodoText"
      id="new-todo"
      placeholder="E.g. Feed the cat"
    >
    <button>Add</button>
  </form>
  <ul>
    <li
      is="todo-item"
      v-for="(todo, index) in todos"
      v-bind:key="todo.id"
      v-bind:title="todo.title"
      v-on:remove="todos.splice(index, 1)"
    ></li>
  </ul>
</div>
Note the is="todo-item" attribute. This is necessary in DOM templates, because only an <li> element is valid inside a <ul>. It does the same thing as <todo-item>, but works around a potential browser parsing error. See DOM Template Parsing Caveats to learn more.

Vue.component('todo-item', {
  template: '\
    <li>\
      {{ title }}\
      <button v-on:click="$emit(\'remove\')">Remove</button>\
    </li>\
  ',
  props: ['title']
})

new Vue({
  el: '#todo-list-example',
  data: {
    newTodoText: '',
    todos: [
      {
        id: 1,
        title: 'Do the dishes',
      },
      {
        id: 2,
        title: 'Take out the trash',
      },
      {
        id: 3,
        title: 'Mow the lawn'
      }
    ],
    nextTodoId: 4
  },
  methods: {
    addNewTodo: function () {
      this.todos.push({
        id: this.nextTodoId++,
        title: this.newTodoText
      })
      this.newTodoText = ''
    }
  }
})