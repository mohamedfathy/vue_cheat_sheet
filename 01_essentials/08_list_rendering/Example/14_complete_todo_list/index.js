Vue.component('todo-item', {
  template: '<li>{{title}} <button v-on:click="$emit(\'remove\')">Remove</button></li>',
  props: ['title']
})

new Vue({
  el: '#app',
  data: {
    newTodoText: '',
    todos: [
      {id:1, title:'Do the dishes.'},
      {id:2, title:'Watch TV.'},
      {id:3, title:'Listen to Music'},
      {id:4, title:'Finish Vue Course.'},
    ],
    nextTodoId: 4
  },
  methods: {
    addNewTodo: function () {
      this.todos.push({
        id: this.newTodoId++,
        title: this.newTodoText
      })
      this.newTodoText = '';
    }
  }
});