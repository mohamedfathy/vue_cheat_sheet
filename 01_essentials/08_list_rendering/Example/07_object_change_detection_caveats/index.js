var vm = new Vue({
  el:"#app",
  data:{
    // vm.a is now reactive
    a:1,
    userProfile: {
      name: 'Mohamed'
    },
    friendProfile: {
      name: 'Mohamed'
    }
  }
});

// vm.b is NOt reactive 
vm.b = 2;

// adding property to object as reactive
Vue.set(vm.userProfile, 'age', 27);

// you can also use vm.$set
vm.$set(vm.userProfile, 'gender', 'male');

// sometimes you may assign number of properties you may use Object.assign()
Object.assign(vm.friendProfile, {
  age:28,
  favoriteColor: 'Green'
})
