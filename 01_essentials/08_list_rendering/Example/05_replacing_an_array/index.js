var vm = new Vue({
  el:"#app",
  data:{
    words: [
    {id:1, name:'category'},
    {id:2, name:'osama'},
    {id:3, name:'cat'},
    {id:4, name:'Aymen'},
    ]
  },
  methods: {
    refineResults: function () {
      this.words = this.words.filter(function (item){
        return item.name.match(/cat/);
      });
    }
  }
});
