var vm = new Vue({
  el:"#app",
  data:{
    items: ['a', 'b', 'c']
  }
});
// this is NOT reactive.
vm.items[1] = 'x';
// this is NOT reactive.
vm.items.length = 2 ;

//Vue.set(vm.items, 1, 'x');
vm.items.splice(1, 1, 'v')
