Vue.component('my-component', {
  template: '<p>my first component</p>'
});

Vue.component('dynamic-component', {
  props: ['todo'],
  template: '<p>{{todo.message}}</p>'
});

var vm = new Vue({
  el:"#app",
  data:{
    todos: [
      {id:1, message: "learn vue", completed: true},
      {id:2, message: "learn java script", completed: false},
      {id:3, message: "do the home work", completed: true}
    ]
  }
});
