var vm = new Vue({
  el:"#app",
  data:{
    todos: [
      {id:1, message: "learn vue", completed: true},
      {id:2, message: "learn java script", completed: false},
      {id:3, message: "do the home work", completed: true},
      {id:4, message: "make interview.", completed: true},
    ]
  }
});
