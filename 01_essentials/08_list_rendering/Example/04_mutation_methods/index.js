var vm = new Vue({
  el:"#app",
  data:{
    users: [
    {id:1, name:'Mohamed'},
    {id:2, name:'Osama'},
    {id:3, name:'Ahmed'},
    {id:4, name:'Aymen'},
    ]
  }
});

vm.users.push({id:9,name:'Sarah'});
vm.users.pop();
vm.users.shift();
vm.users.unshift({id:8,name:'Gamal'});
vm.users.reverse();