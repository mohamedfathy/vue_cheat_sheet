Vue.component('my-component', {
  template: '<li>{{item.title}}</li>',
  props: ['item']
})

var vm = new Vue({
  el:"#app",
  data: {
    todos: [
      {id:1, title:'Do the dishes.'},
      {id:2, title:'Watch TV.'},
      {id:3, title:'Listen to Music'},
      {id:4, title:'Finish Vue Course.'},
    ]
  }
});
