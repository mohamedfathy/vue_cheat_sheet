Vue.component('todo-item', {
  props : ['todo'],
  template: '<li>{{todo.text}}</li>'
})

var app = new Vue({
    el:'#app',
    data: {
      groceryList : [
        {id:1, text: 'Vegatables'},
        {id:2, text: 'Cheese'},
        {id:3, text: 'Meat'}
      ]
    }
});