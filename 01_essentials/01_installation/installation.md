### Using Vue DevTools
- Devtools allowing you to inspect and debug applications.

### Direct Script include
- Simply download and include with a script tag

### CDN
- For prototyping or learning purposes, you can use the latest version with:
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

- For production, we recommend linking to a specific version number and build to avoid unexpected breakage from newer versions:
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.0"></script>