var vm = new Vue({
  el:"#app",
  data:{
    loginType: 'user'
  },
  methods: {
    changeType : function () {
      this.loginType = this.loginType === 'user' ? 'email' : 'user'
    }
  }
});