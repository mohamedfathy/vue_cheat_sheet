vm = new Vue({
  el: "#app",
  data:{
    attributeName: "href",
    eventName: "click",
    url: "https://www.google.com"
  },
  methods : {
    doSomething : function () {
      alert("You did something.");
    }
  }
});