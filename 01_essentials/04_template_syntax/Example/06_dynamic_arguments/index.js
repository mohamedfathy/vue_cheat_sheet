vm = new Vue({
  el: "#app",
  data:{
    attributeName: "href",
    url: "https://www.google.com",
    eventName: "click"
  },
  methods : {
    doSomething: function () {
      alert("You Clicked this button");
    }
  }
});