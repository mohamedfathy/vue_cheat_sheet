# Template Syntax

## Interpolations

### Text
- You can bind data is using the “Mustache” syntax (double curly braces)

```
<span>Message: {{ msg }}</span>
```

### One-Time Interpolations
- one-time interpolations means data do not change after first interpolation.

```
<span v-once>This will never change: {{ msg }}</span>
```

### Raw HTML
- In order to output real HTML, you will need to use the v-html directive:

```
<p><span v-html="rawHtml"></span></p>
```

### Attributes
- Mustaches cannot be used inside HTML attributes. Instead, use a v-bind directive:

```
<div v-bind:id="dynamicId"></div>
```

### Boolean Attributes
- boolean attributes, where their mere existence implies true, v-bind works a little differently. If isButtonDisabled has the value of null, undefined, or false, the disabled attribute will not even be included in the rendered <button> element.

```
<button v-bind:disabled="isButtonDisabled">Button</button>
```

### JavaScript Expressions
- These expressions will be evaluated as JavaScript in the data scope of the owner Vue instance. 

```
{{ number + 1 }}

{{ ok ? 'YES' : 'NO' }}

{{ message.split('').reverse().join('') }}

<div v-bind:id="'list-' + id"></div>
```

- binding can only contain one single expression, so the following will NOT work:

```
<!-- this is a statement, not an expression: -->
{{ var a = 1 }}

<!-- flow control won't work either, use ternary expressions -->
{{ if (ok) { return message } }}
```

## Directives
- Directives are special attributes with the v- prefix. A directive’s job is to reactively apply side effects to the DOM when the value of its expression changes. 

```
<p v-if="seen">Now you see me</p>
```

### Arguments
- Some directives can take an “argument”, denoted by a colon after the directive name.

- v-bind directive is used to reactively update an HTML attribute:

```
<a v-bind:href="url"> ... </a>
```

- v-on directive, which listens to DOM events:

```
<a v-on:click="doSomething"> ... </a>
```

### Dynamic Arguments
- use a JavaScript expression in a directive argument by wrapping it with square brackets:

```
<a v-bind:[attributeName]="url"> ... </a>
<a v-on:[eventName]="doSomething"> ... </a>
```

#### Dynamic Argument Value Constraints
- Dynamic arguments should be string, with the exception of null. The special value null can be used to explicitly remove the binding. Any other non-string value will trigger a warning.

#### Dynamic Argument Expression Constraints
- spaces and quotes and uppercase characters, are invalid inside HTML attribute names.

```
<!-- This will trigger a compiler warning. -->
<a v-bind:['foo' + bar]="value"> ... </a>
```

### modifiers
- Modifiers are special postfixes denoted by a dot, which indicate that a directive should be bound in some special way.

```
<form v-on:submit.prevent="onSubmit"> ... </form>
```

## Shorthands

### v-bind Shorthand
```
<!-- full syntax -->
<a v-bind:href="url"> ... </a>

<!-- shorthand -->
<a :href="url"> ... </a>
```

### v-on Shorthand
```
<!-- full syntax -->
<a v-on:click="doSomething"> ... </a>

<!-- shorthand -->
<a @click="doSomething"> ... </a>
```