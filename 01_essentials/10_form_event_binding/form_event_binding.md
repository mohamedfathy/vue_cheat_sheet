# Form Input Bindings

### Basic Usage
- You can use the v-model directive to create two-way data bindings on form input, textarea, and select elements.

- v-model will ignore the initial value, checked or selected attributes found on any form elements. It will always treat the Vue instance data as the source of truth.

- text and textarea elements use value property and input event;
- checkboxes and radiobuttons use checked property and change event;
- select fields use value as a prop and change as an event.


### Text

```
<input v-model="message" placeholder="edit me">
<p>Message is: {{ message }}</p>
```

### Multiline text
- Interpolation on textareas (<textarea>{{text}}</textarea>) won't work. Use v-model instead.

```
<span>Multiline message is:</span>
<p style="white-space: pre-line;">{{ message }}</p>
<br>
<textarea v-model="message" placeholder="add multiple lines"></textarea>
```

### Checkbox
- Single checkbox, boolean value.

```
<input type="checkbox" id="checkbox" v-model="checked">
<label for="checkbox">{{ checked }}</label>
```

- Multiple checkboxes, bound to the same Array.

```
<div id='example-3'>
  <input type="checkbox" id="jack" value="Jack" v-model="checkedNames">
  <label for="jack">Jack</label>
  <input type="checkbox" id="john" value="John" v-model="checkedNames">
  <label for="john">John</label>
  <input type="checkbox" id="mike" value="Mike" v-model="checkedNames">
  <label for="mike">Mike</label>
  <br>
  <span>Checked names: {{ checkedNames }}</span>
</div>
new Vue({
  el: '#example-3',
  data: {
    checkedNames: []
  }
})
```

### Radio
```
<input type="radio" id="one" value="One" v-model="picked">
<label for="one">One</label>
<br>
<input type="radio" id="two" value="Two" v-model="picked">
<label for="two">Two</label>
<br>
<span>Picked: {{ picked }}</span>
```

### Select
#### Single select:

- If the initial value of your v-model expression does not match any of the options, the <select> element will render in an “unselected” state. On iOS this will cause the user not being able to select the first item because iOS does not fire a change event in this case. It is therefore recommended to provide a disabled option with an empty value, as demonstrated in the example above.

```
<select v-model="selected">
  <option disabled value="">Please select one</option>
  <option>A</option>
  <option>B</option>
  <option>C</option>
</select>
<span>Selected: {{ selected }}</span>
new Vue({
  el: '...',
  data: {
    selected: ''
  }
})
```

#### Multiple select (bound to Array):

```
<select v-model="selected" multiple>
  <option>A</option>
  <option>B</option>
  <option>C</option>
</select>
<br>
<span>Selected: {{ selected }}</span>
```

### Dynamic options rendered with v-for:

```
<select v-model="selected">
  <option v-for="option in options" v-bind:value="option.value">
    {{ option.text }}
  </option>
</select>
<span>Selected: {{ selected }}</span>
new Vue({
  el: '...',
  data: {
    selected: 'A',
    options: [
      { text: 'One', value: 'A' },
      { text: 'Two', value: 'B' },
      { text: 'Three', value: 'C' }
    ]
  }
})
```

### Value Bindings
#### Static binding Using v-model
- For radio and select options, the v-model binding values are usually static strings.
- For checkbox, the v-model binding values are usually  booleans.

```
<p>Radio </p>
// when value of picked matches a then a will be selected.
<input type="radio" v-model="picked" value="a">a
<input type="radio" v-model="picked" value="b">b

// when value of toggle is true then input will be selected.
<!-- `toggle` is either true or false -->
<input type="checkbox" v-model="toggle">

// when value of selected is abc then will be selected.
<select v-model="selected">
  <option value="abc">ABC</option>
</select>
```


### Checkbox
- The true-value and false-value attributes don’t affect the input’s value attribute, because browsers don’t include unchecked boxes in form submissions. To guarantee that one of two values is submitted in a form (e.g. “yes” or “no”), use radio inputs instead.

```
<input
  type="checkbox"
  v-model="toggle"
  true-value="yes"
  false-value="no"
>
// when checked:
vm.toggle === 'yes'
// when unchecked:
vm.toggle === 'no'
```

#### Dynamic binding Using v-bind
- But sometimes we may want to bind the value to a dynamic property on the Vue instance. We can use v-bind to achieve that. In addition, using v-bind allows us to bind the input value to non-string values.


### Radio
```
<input type="radio" v-model="pick" v-bind:value="a">
// when checked:
vm.pick === vm.a

Select Options
<select v-model="selected">
  <!-- inline object literal -->
  <option v-bind:value="{ number: 123 }">123</option>
</select>
// when selected:
typeof vm.selected // => 'object'
vm.selected.number // => 123
```

### Modifiers
#### .lazy
- By default, v-model syncs the input with the data after each input event. You can add the lazy modifier to instead sync after change events:
```
<!-- synced after "change" instead of "input" -->
<input v-model.lazy="msg" >
```

#### .number
- number modifier automatically typecast as a number.

```
<input v-model.number="age" type="number">
```

#### .trim
- trim modifier automatically trims whitespaces.

```
<input v-model.trim="msg">
```