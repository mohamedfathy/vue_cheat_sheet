var vm = new Vue({
  el:"#app",
  data:{
    selected: 'A',
    multiSelect:[],
    dynamicSelect: 'A',
    options: [
      {text: 'One', value: 'A'},
      {text: 'Two', value: 'B'},
      {text: 'Three', value: 'C'},
      {text: 'Four', value: 'D'},
    ]
  } 
});