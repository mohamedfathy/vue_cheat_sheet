var vm = new Vue({
  el:"#app",
  data:{
    // radio button values
    d:'d',
    g:'g',
    f:'f',
    // radio button final values
    staticPicked:'',
    dynamicPicked: '',

    // checkbox values
    carOne: 'Bmw',
    carTwo: 'Mercedes',

    // checkbox final values
    staticCheckbox:'',
    cars: [],

    // select list values
    x:'x',
    y:'y',
    z:'z',
    // select list final values
    staticSelected:'',
    dynamicSelected: '',
  } 
});

console.log(typeof vm.selected);