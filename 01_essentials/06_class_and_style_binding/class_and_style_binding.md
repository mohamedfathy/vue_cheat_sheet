# Class and Style Bindings

## Class bindings

### Object Syntax
- We can pass an object to v-bind:class to dynamically toggle classes:
- The above syntax means the presence of the active class will be determined by the truthiness of the data property isActive.

```
<div v-bind:class="{ active: isActive }"></div>

data: {
  isActive: true
}

- It will render:
<div class="static active"></div>
```

### With Components

```
Vue.component('my-component', {
  template: '<p class="foo bar">Hi</p>'
})

// Then add some classes when using it
<my-component class="baz boo"></my-component>

The rendered HTML will be:
<p class="foo bar baz boo">Hi</p>
```

### Inline Styles Binding

### Object Syntax
- The object syntax for v-bind:style is pretty straightforward - it looks almost like CSS, except it’s a JavaScript object. You can use either camelCase or kebab-case (use quotes with kebab-case) for the CSS property names:

```
<div v-bind:style="{ color: activeColor, fontSize: fontSize + 'px' }"></div>
data: {
  activeColor: 'red',
  fontSize: 30
}
```

### Auto-prefixing (vendor prefixes)
- Vue will automatically detect and add appropriate prefixes to the applied styles.

### Multiple Values
- Starting in 2.3.0+ you can provide an array of multiple (prefixed) values to a style property, for example:

- This will only render the last value in the array which the browser supports. In this example, it will render display: flex for browsers that support the unprefixed version of flexbox.

```
<div v-bind:style="{ display: ['-webkit-box', '-ms-flexbox', 'flex'] }"></div>
```