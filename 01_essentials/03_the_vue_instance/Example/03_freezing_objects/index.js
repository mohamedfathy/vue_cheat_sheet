var obj = {
  name: 'Mohamed'
}

Object.freeze(obj);

new Vue({
  el: "#app",
  data: obj
});