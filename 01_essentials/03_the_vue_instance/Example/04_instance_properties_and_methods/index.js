var data = {a:1};
var vm = new Vue({
  el: '#app',
  data: data
});

// true
if (vm.$data === data) {console.log('they are equal.')}
// true
if (vm.$el === document.getElementById('app')) {console.log('they are equal too.')}

vm.$watch('a', function(newValue, oldValue){
  // This callback will be called when 'vm.a' changes.
  alert('Value of a Changed.')
});
