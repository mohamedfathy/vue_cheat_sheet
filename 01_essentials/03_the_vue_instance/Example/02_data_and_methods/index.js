// our data object
var data = {a:1};

// the object is added to the vue instance.
var vm = new Vue({
  el: '#app',
  data:data
});

// Getting the property on instance 
// return the one from original data
vm.a == data.a // returns true

// setting the property in instance 
// also affects the original data
vm.a = 2;
data.a // => 2

// and vice versa 
data.a = 3;
vm.a // => 3

// this will not be reactive.
vm.b = "hi";
