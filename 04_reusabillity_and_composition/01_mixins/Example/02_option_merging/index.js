var mixin = {
  data: function () {
    return {
      message: 'Hello',
      foo: 'abc'
    }
  }
}

new Vue({
  mixins: [mixin],
  data: function () {
    return { 
      message: 'Good Bye',
      bar: 'def'
    }
  },
  created: function () {
    console.log(this.$data);
  }
})