var mixin = {
  methods: {
    foo: function () {
      console.log('foo');
    },
    conflicating: function () {
      console.log('From Mixin');
    }
  }
}

let vm = new Vue({
  mixins: [mixin],
  methods: {
    bar: function () {
      console.log('bar');
    },
    conflicating: function () {
      console.log('from Component');
    }
  },

})

vm.bar()
vm.foo()
vm.conflicating()