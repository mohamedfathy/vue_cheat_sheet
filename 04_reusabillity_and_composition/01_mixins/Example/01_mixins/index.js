// define a mixin object
var myMixin = {
  created: function () {
    this.hello();
  },
  methods: {
    hello:function () {
      console.log('Hello from Mixins!');
    }
  }
}

var Component = Vue.extend({
  mixins: [myMixin]
})
var component = new Component();