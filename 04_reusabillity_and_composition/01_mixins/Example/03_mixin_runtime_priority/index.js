var mixin = {
  created: function () {
    console.log('Mixin hook Called!');
  }
}

new Vue({
  mixins:[mixin],
  created: function (){
    console.log('Component hook Called');
  }
})