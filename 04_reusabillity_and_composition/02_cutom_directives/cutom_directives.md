# Custom Directives

### Intro
- Vue also allows you to register your own custom directives.
- there may be cases where you need some low-level DOM access on plain elements.

```
// Register a global custom directive called `v-focus`
Vue.directive('focus', {
  // When the bound element is inserted into the DOM...
  inserted: function (el) {
    // Focus the element
    el.focus()
  }
})

// register a directive locally instead
directives: {
  focus: {
    // directive definition
    inserted: function (el) {
      el.focus()
    }
  }
}

<input v-focus>
```

### Hook Functions

```
bind
inserted
update
componentUpdate
unbind
```

### Directive Hook Arguments

new Vue({
  mixins: [mixin],
  data: function () {
    return {
      message: 'goodbye',
      bar: 'def'
    }
  },
  created: function () {
    console.log(this.$data)
    // => { message: "goodbye", foo: "abc", bar: "def" }
  }
})
```

### Global Mixin
- Once you apply a mixin globally, it will affect every Vue instance created afterwards.

```
// inject a handler for `myOption` custom option
Vue.mixin({
  created: function () {
    var myOption = this.$options.myOption
    if (myOption) {
      console.log(myOption)
    }
  }
})

new Vue({
  myOption: 'hello!'
})
// => "hello!"

```

### Merge Strategies
- Will come back Later.