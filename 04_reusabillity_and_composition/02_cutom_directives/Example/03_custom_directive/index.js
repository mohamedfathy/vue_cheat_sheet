Vue.directive('demo', {
  bind: function (el, binding, vnode) {
    var s = JSON.stringify
    el.innerHTML = 
    'name: ' + s(binding.name) + '<br>' +
    'value: ' + s(binding.value) + '<br>' +
    'expession: ' + s(binding.expression) + '<br>' +
    'argument: ' + s(binding.arg) + '<br>'  +
    'modifiers: ' + s(binding.modifiers) + '<br>' +
    'v-node keys: ' + Object.keys(vnode).join(', ')

  }
})

new Vue({
  el: '#hook-arguments-example',
  data: {
    message: 'hello'
  }
})