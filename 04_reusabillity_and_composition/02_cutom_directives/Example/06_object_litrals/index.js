Vue.directive('demo', function (el, binding) {
  el.style.backgroundColor = binding.value.color;
  el.style.innerHTML= binding.value.text;
})

new Vue({
  el: '#app',
  data: {
    green:'green'
  }
})