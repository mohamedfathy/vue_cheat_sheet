Vue.component('input-component', {
  template: `
  <input type="text" v-focus>
  `,
  directives: {
    focus: {
      inserted: function (el) {
        el.focus()
      }
    }
  }
})
let vm = new Vue({
  el: '#app',
  
})