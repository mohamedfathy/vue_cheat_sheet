# Component Registration

### Component Names
- The component’s name should be all-lowercase, must contain a hyphen.

```
Vue.component('my-component-name', { /* ... */ })
```

### Name Casing
```
// With kebab-case
Vue.component('my-component-name', { /* ... */ })
// With PascalCase
Vue.component('MyComponentName', { /* ... */ })
```

### Global Registration
- globally registered components can be used in any Vue instance created after registration.

```
Vue.component('component-a', { /* ... */ })

new Vue({ el: '#app' })

<div id="app">
  <component-a></component-a>
</div>

```

### Local Registration
- Global registration increases the amount of JavaScript your users have to download.
```
var ComponentA = { /* ... */ }
var ComponentB = { /* ... */ } 

new Vue({
  el: '#app',
  components: {
    'component-a': ComponentA,
    'component-b': ComponentB
  }
}) 
```

- locally registered components are not also available in subcomponents. For example, if you wanted ComponentA to be available in ComponentB, you’d have to use:

```
var ComponentA = { /* ... */ }

var ComponentB = {
  components: {
    'component-a': ComponentA
  },
  // ...
}
```

### Module Systems
- Will come back Later.