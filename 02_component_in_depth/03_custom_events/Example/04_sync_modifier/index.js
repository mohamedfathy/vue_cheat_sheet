Vue.component('blog-post', {
  template: `
    <div class="blog-post">
      <h3>Post Title</h3>
    </div>
  `
});

var app = new Vue({
    el:'#app',
    methods:{ 
      doSomething: function () {
        alert('You clicked the component.');
      }
    }
});