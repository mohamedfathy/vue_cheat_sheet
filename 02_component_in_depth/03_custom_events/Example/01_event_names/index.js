Vue.component('blog-post', {
  template: `
    <div>
      <h3>Post Title</h3>
      <button v-on:click="$emit('my-event')">Read More</button>
    </div>
  `
});

var app = new Vue({
    el:'#app',
    methods:{ 
      doSomething: function () {
        alert('Hello How are you.');
      }
    }
});