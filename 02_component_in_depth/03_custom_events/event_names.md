# Custom Events

### Event Names
- event names should be written in kabab case.

```
this.$emit('my-event')
<my-component v-on:my-event="doSomething"></my-component>
```

### Customizing Component v-model
- Not understood yet.

### Binding Native Events to Components
- Not understood yet.

### .sync Modifier
- Not understood yet.