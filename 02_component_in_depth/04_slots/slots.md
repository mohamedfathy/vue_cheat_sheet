# Slots

### Slot Content
- slot element is used to serve as distribution outlets for content.

```
<navigation-link url="/profile"> Your Profile </navigation-link>

template: `
  <a v-bind:href="url">
    <slot></slot>
  </a>
`
```

### Compilation Scope
```
<navigation-link href="www.google.com">Logged as {{user.name}}</navigation-link>

```

### Fallback Content
```
<button><slot>Submit<slot></button>
```

### Named Slots
- it’s useful to have multiple slots.
- A <slot> outlet without name implicitly has the name “default”.
- we can use the v-slot directive on a <template>

```
<div class="container">
  <header>
    <slot name="header"></slot>
  </header>
  <main>
    <slot></slot>
  </main>
  <footer>
    <slot name="footer"></slot>
  </footer>
</div>

<base-layout>
  <template v-slot:header>
    <h1>Here might be a page title</h1>
  </template>

  <template v-slot:default>
    <p>A paragraph for the main content.</p>
    <p>And another one.</p>
  </template>

  <template v-slot:footer>
    <p>Here's some contact info</p>
  </template>
```

### Scoped Slots
- Not understood yet.

### Dynamic Slot Names
- Dynamic directive arguments also work on v-slot, allowing the definition of dynamic slot names.
```
Vue.component('base-layout', {
  template: `
    <div class="container">
      <header>
        <slot name="header">Header</slot>
      </header>
      <main>
        <slot name="default">Main</slot>
      </main>
      <footer>
        <slot name="footer">Footer</slot>
      </footer>
    </div>
  `
});

var app = new Vue({
    el:'#app',
    data: {
      dynamic_slot_name: 'header'
    }
});

<base-layout>
    <template v-slot:[dynamic_slot_name]>New Header</template>
</base-layout>
```

### Named Slots Shorthand
- v-slot also has a shorthand, replacing with #.

```
<template #header>
  <h1>Here might be a page title</h1>
</template>

<template v-solt="header">
  <h1>Here might be a page title</h1>
</template>
```

### Deprecated Syntax
The v-slot directive was introduced in Vue 2.6.0, offering an improved, alternative API to the still-supported slot and slot-scope attributes. The full rationale for introducing v-slot is described in this RFC. The slot and slot-scope attributes will continue to be supported in all future 2.x releases, but are officially deprecated and will eventually be removed in Vue 3.

Named Slots with the slot Attribute
Deprecated in 2.6.0+. See here for the new, recommended syntax.

To pass content to named slots from the parent, use the special slot attribute on <template> (using the <base-layout> component described here as example):

<base-layout>
  <template slot="header">
    <h1>Here might be a page title</h1>
  </template>

  <p>A paragraph for the main content.</p>
  <p>And another one.</p>

  <template slot="footer">
    <p>Here's some contact info</p>
  </template>
</base-layout>
Or, the slot attribute can also be used directly on a normal element:

<base-layout>
  <h1 slot="header">Here might be a page title</h1>

  <p>A paragraph for the main content.</p>
  <p>And another one.</p>

  <p slot="footer">Here's some contact info</p>
</base-layout>
There can still be one unnamed slot, which is the default slot that serves as a catch-all for any unmatched content. In both examples above, the rendered HTML would be:

<div class="container">
  <header>
    <h1>Here might be a page title</h1>
  </header>
  <main>
    <p>A paragraph for the main content.</p>
    <p>And another one.</p>
  </main>
  <footer>
    <p>Here's some contact info</p>
  </footer>
</div>
Scoped Slots with the slot-scope Attribute
Deprecated in 2.6.0+. See here for the new, recommended syntax.

To receive props passed to a slot, the parent component can use <template> with the slot-scope attribute (using the <slot-example> described here as example):

<slot-example>
  <template slot="default" slot-scope="slotProps">
    {{ slotProps.msg }}
  </template>
</slot-example>
Here, slot-scope declares the received props object as the slotProps variable, and makes it available inside the <template> scope. You can name slotProps anything you like similar to naming function arguments in JavaScript.

Here slot="default" can be omitted as it is implied:

<slot-example>
  <template slot-scope="slotProps">
    {{ slotProps.msg }}
  </template>
</slot-example>
The slot-scope attribute can also be used directly on a non-<template> element (including components):

<slot-example>
  <span slot-scope="slotProps">
    {{ slotProps.msg }}
  </span>
</slot-example>
The value of slot-scope can accept any valid JavaScript expression that can appear in the argument position of a function definition. This means in supported environments (single-file components or modern browsers) you can also use ES2015 destructuring in the expression, like so:

<slot-example>
  <span slot-scope="{ msg }">
    {{ msg }}
  </span>
</slot-example>
Using the <todo-list> described here as an example, here’s the equivalent usage using slot-scope:

<todo-list v-bind:todos="todos">
  <template slot="todo" slot-scope="{ todo }">
    <span v-if="todo.isComplete">✓</span>
    {{ todo.text }}
  </template>
</todo-list>