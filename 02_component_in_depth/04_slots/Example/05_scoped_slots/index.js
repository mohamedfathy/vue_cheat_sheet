Vue.component('current-user', {
  props: ['user'],
  template: `
    <span>
      <slot>{{user.firstName}}</slot>
    </span>
  `
});

var app = new Vue({
    el:'#app',
    data: {
      user: {
        firstName: "Mohamed",
        lastName: "Fathy"
      }
    }
});