Vue.component('navigation-link', {
  template: `
    <a v-bind:href="url">
      <slot></slot>
    </a>
  `
});

var app = new Vue({
    el:'#app'
});