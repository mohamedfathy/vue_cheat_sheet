Vue.component('base-layout', {
  template: `
    <div class="container">
      <header>
        <slot name="header">Header</slot>
      </header>
      <main>
        <slot name="default">Main</slot>
      </main>
      <footer>
        <slot name="footer">Footer</slot>
      </footer>
    </div>
  `
});

var app = new Vue({
    el:'#app',
});