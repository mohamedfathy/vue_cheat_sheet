Vue.component('navigation-link', {
  props: ['url'],
  template: `
    <a v-bind:href="url">
      <slot></slot>
    </a>
  `
});

var app = new Vue({
    el:'#app',
    data: {
      user: {
        name: 'Mohamed'
      }
    }
});