# Props

### Prop Casing (camelCase vs kebab-case)
- always use camelCased version in javascript and use the kebab case version in html.

```
Vue.component('blog-post', {
  // camelCase in JavaScript
  props: ['postTitle'],
  template: '<h3>{{ postTitle }}</h3>'
})
<!-- kebab-case in HTML -->
<blog-post post-title="hello!"></blog-post>
```

### Prop Types
- we’ve only seen props listed as an array of strings. you can list props as an object also.

```
props: ['title', 'likes', 'isPublished', 'commentIds', 'author']
props: {
  title: String,
  likes: Number,
  isPublished: Boolean,
  commentIds: Array,
  author: Object,
  callback: Function,
  contactsPromise: Promise // or any other constructor
}
```


### Passing Static or Dynamic Props
  #### Passing a Number
  ```
  <blog-post v-bind:likes="42"></blog-post>
  <blog-post v-bind:likes="post.likes"></blog-post>
  ```

  #### Passing a Boolean
  ```
  <blog-post is-published></blog-post>
  <blog-post v-bind:is-published="false"></blog-post>
  <blog-post v-bind:is-published="post.isPublished"></blog-post>
  ```

  #### Passing an Array
  ```
  <blog-post v-bind:comment-ids="[234, 266, 273]"></blog-post>
  <blog-post v-bind:comment-ids="post.commentIds"></blog-post>
  ```

  #### Passing an Object
  ```
  <blog-post
    v-bind:author="{
      name: 'Veronica',
      company: 'Veridian Dynamics'
    }"
  ></blog-post>

  <blog-post v-bind:author="post.author"></blog-post>
  ```

  #### Passing the Properties of an Object
```
<blog-post v-bind="post"></blog-post>
```

### One-Way Data Flow
- All props form a one-way-down binding between the child property and the parent one.

- The prop is used to pass in an initial value; the child component wants to use it as a local data property afterwards.

```
props: ['initialCounter'],
data: function () {
  return {
    counter: this.initialCounter
  }
}
``` 
- The prop is passed in as a raw value that needs to be transformed.

```
props: ['size'],
computed: {
  normalizedSize: function () {
    return this.size.trim().toLowerCase()
  }
}
``` 

- Note that objects and arrays in JavaScript are passed by reference, so if the prop is an array or object, mutating the object or array itself inside the child component will affect parent state.


### Prop Validation
- To specify prop validations, you can provide an object with validation requirements.

```
Vue.component('my-component', {
  props: {
    // Basic type check
    propA: Number,
    // Multiple possible types
    propB: [String, Number],
    // Required string
    propC: {
      type: String,
      required: true
    },
    // Number with a default value
    propD: {
      type: Number,
      default: 100
    },
    // Object with a default value
    propE: {
      type: Object,
      // Object or array defaults must be returned from
      // a factory function
      default: function () {
        return { message: 'hello' }
      }
    },
    // Custom validator function
    propF: {
      validator: function (value) {
        // The value must match one of these strings
        return ['success', 'warning', 'danger'].indexOf(value) !== -1
      }
    }
  }
})
``` 

### Non-Prop Attributes
- A non-prop attribute is an attribute that is passed to a component, but does not have a corresponding prop defined.

###Replacing/Merging with Existing Attributes 
- both main-style and form-control values are merged.

```
Vue.component('', {
  template: "<input type="date" class="form-control">"
})

<blog-post class="main-style" ></blog-post>
```

### Disabling Attribute Inheritance
- If you do not want the root element of a component to inherit attributes, you can set inheritAttrs: false in the component’s options. For example:

```
Vue.component('my-component', {
  inheritAttrs: false,
  // ...
})
```