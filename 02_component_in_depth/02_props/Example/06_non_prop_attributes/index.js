Vue.component('blog-post', {
  props: {
    title: String,
    likes: Number,
    isPublished: Boolean,
    commentIds: Array, 
    author: Object,
    callback: Function,
    contactPromise: Promise
  }, 
  template: `
    <div class="blog-post">
      <h1>{{title}}</h1>
      <p>{{likes}}</p>
      <p v-for="item in commentIds">{{item}}</p>
    </div>
  `
});

var app = new Vue({
    el:'#app',
    data: {
      post: {
        likes: 88,
        isPublished: true,
        commentIds: [2,3,5]
      }
    }
});