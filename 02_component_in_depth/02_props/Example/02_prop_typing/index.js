Vue.component('blog-post', {
  props: {
    title: String,
    likes: Number,
    isPublished: Boolean,
    commentIds: Array, 
    author: Object,
    callback: Function,
    contactPromise: Promise
  }, 
  template: `
    <div class="blog-post">
      <h1>{{title}}</h1>
      <p>{{likes}}</p>
    </div>
  `
});

var app = new Vue({
    el:'#app'
});