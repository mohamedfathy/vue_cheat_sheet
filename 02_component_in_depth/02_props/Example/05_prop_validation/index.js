Vue.component('blog-post', {
  props: {
    title: {type:String,required:true},
    likes: Number,
    views: {type:Number, default:100}
  }, 
  template: `
    <div class="blog-post">
      <h1>{{title}}</h1>
      <p>{{likes}}</p>
    </div>
  `
});

var app = new Vue({
    el:'#app',
    data: {
        title: "Death in Vince.",
        likes: 88,
        isPublished: true,
        commentIds: [2,3,5]
    }
});