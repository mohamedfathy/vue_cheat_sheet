Vue.component('blog-post', {
  props: ['title'],
  data: function () {
    return {
      name: this.title
    }
  },
  template: `
  <div>
    <p>Normal: {{name}}</p>
    <p>Altered: {{finalName}}</p>
  </div>
  `,
  computed: {
    finalName: function () {
      return this.name.toUpperCase();s
    }
  }
});

var app = new Vue({
    el:'#app',
    data: {
      title: "Esraa"
    }
});