Vue.component('number-component', {
  props: {
    likes: Number,
  }, 
  template: `
    <div>
      <span>Number type: {{likes}}</span>
    </div>
  `
});

Vue.component('string-component', {
  props: {
    title: String,
  }, 
  template: `
    <div>
      <span>String type: {{title}}</span>
    </div>
  `
});


Vue.component('boolean-component', {
  props: {
    isPublished: Boolean,
  }, 
  template: `
    <div>
      <span>Boolean type: {{isPublished}}</span>
    </div>
  `
});

Vue.component('array-component', {
  props: {
    commentIds: Array,
  }, 
  template: `
    <div>
      <span>Array type:</span>
      <li v-for="item in commentIds" v-bind:key="item">{{item}}</li>
    </div>
  `
});

Vue.component('object-component', {
  props: {
    author: Object,
  }, 
  template: `
    <div>
      <span>Object type:</span>
      <li v-for="value in author">{{value}}</li>
    </div>
  `
});


var app = new Vue({
    el:'#app',
    data: {
      post: {
        likes: 88,
        title: "Osama",
        isPublished: true,
        commentIds: [2,3,5], 
        author: {
          name: "Mohamed", 
          title: "Death in Vince."
        }
      }
    }
});