Vue.component('blog-post', {
  props: ['postTitle'], 
  template: `
    <div class="blog-post">
      <h3>{{postTitle}}</h3>
    </div>
  `
});

var app = new Vue({
    el:'#app'
});