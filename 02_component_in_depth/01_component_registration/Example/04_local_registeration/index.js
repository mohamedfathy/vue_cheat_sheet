let componentOne = {
  template : '<p>Component one with local registeration.</p>'
}

let componentThree = {
  template: '<p>component three</p>'
}

let componentFour = {
  components: {
    'component-three': componentThree
  },
  template: '<p><component-three></component-three> compontent four </p>'
}

var app = new Vue({
    el:'#app',
    components: {
      'component-one': componentOne, 
      'component-three': componentThree,
      'component-four': componentFour
    } 
});