Vue.component('base-layout', {
  template: `
    <div class="container">
      <header>
        Header content goes here...
        {{this.$root.foo}}
      </header>
      <main>
        Main content goes here...
      </main>
      <footer>
        Footer content goes here...
      </footer>
    </div> 
  `
});

var app = new Vue({
    el:'#app',
    data: {
      foo: 'Mohamed'
    },
    methods: {
      baz: function () {}
    },
    computed: {
      bar: function () {}
    }
});