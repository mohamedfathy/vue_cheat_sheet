Vue.component('parent-component', {
  template: `
    <div>
      <h1>Post Title</h1>
      <slot></slot>
    </div>
  `
});

Vue.component('child-component', {
  template: `
    <p>Post Content
    <slot></slot>
    </p>
  `
});

Vue.component('elipsis-component', {
  template: `
    <a href="google.com">{{link}}</a>
  `,
  inject: ['link']
});

var app = new Vue({
    el:'#app',
    provide: function () {
      return {
        link: 'google link'
      }
    }
});