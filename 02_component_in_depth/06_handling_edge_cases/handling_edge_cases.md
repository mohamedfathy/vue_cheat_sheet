# Handling Edge Cases

## Element & Component Access

### Accessing the Root Instance
- In every subcomponent of a new Vue instance, this root instance can be accessed with the $root property.

```
// The root Vue instance
new Vue({
  data: {
    foo: 1
  },
  computed: {
    bar: function () { /* ... */ }
  },
  methods: {
    baz: function () { /* ... */ }
  }
})

// Get root data
this.$root.foo
```

### Accessing the Parent Component Instance
- Similar to $root, the $parent property can be used to access the parent instance from a child. 


### Accessing Child Component Instances & Child Elements
- Despite the existence of props and events, sometimes you might still need to directly access a child component in JavaScript. To achieve this you can assign a reference ID to the child component using the ref attribute. For example:

```
<base-input ref="userNameInput"></base-input>

var app = new Vue({
    el:'#app',
    data: {
      
    }, 
    mounted: function (){
      console.log(this.$refs.userNameInput);
    }
});
```

### Dependency Injection
- In this component, all descendants of <google-map> needed access to a getMap method, in order to know which map to interact with. Unfortunately, using the $parent property didn’t scale well to more deeply nested components. That’s where dependency injection can be useful, using two new instance options: provide and inject.

```
provide: function () {
  return {
    getMap: this.getMap
  }
}

inject: ['getMap']
```

### Programmatic Event Listeners
- So far, you’ve seen uses of $emit, listened to with v-on, but Vue instances also offer other methods in its events interface. We can:

- Listen for an event with $on(eventName, eventHandler)
- Listen for an event only once with $once(eventName, eventHandler)
- Stop listening for an event with $off(eventName, eventHandler)

### Circular References
#### Recursive Components
- recursive components can also lead to infinite loops:
- A component will result in a “max stack size exceeded” error, so make sure recursive invocation is conditional
```
name: 'stack-overflow',
template: '<div><stack-overflow></stack-overflow></div>'
```

#### Circular References Between Components


### Alternate Template Definitions
#### Inline Templates
When the inline-template special attribute is present on a child component, the component will use its inner content as its template, rather than treating it as distributed content. This allows more flexible template-authoring.

<my-component inline-template>
  <div>
    <p>These are compiled as the component's own template.</p>
    <p>Not parent's transclusion content.</p>
  </div>
</my-component>
Your inline template needs to be defined inside the DOM element to which Vue is attached.

However, inline-template makes the scope of your templates harder to reason about. As a best practice, prefer defining templates inside the component using the template option or in a <template> element in a .vue file.

X-Templates
Another way to define templates is inside of a script element with the type text/x-template, then referencing the template by an id. For example:

<script type="text/x-template" id="hello-world-template">
  <p>Hello hello hello</p>
</script>
Vue.component('hello-world', {
  template: '#hello-world-template'
})
Your x-template needs to be defined outside the DOM element to which Vue is attached.

These can be useful for demos with large templates or in extremely small applications, but should otherwise be avoided, because they separate templates from the rest of the component definition.

Controlling Updates
Thanks to Vue’s Reactivity system, it always knows when to update (if you use it correctly). There are edge cases, however, when you might want to force an update, despite the fact that no reactive data has changed. Then there are other cases when you might want to prevent unnecessary updates.

Forcing an Update
If you find yourself needing to force an update in Vue, in 99.99% of cases, you’ve made a mistake somewhere.

You may not have accounted for change detection caveats with arrays or objects, or you may be relying on state that isn’t tracked by Vue’s reactivity system, e.g. with data.

However, if you’ve ruled out the above and find yourself in this extremely rare situation of having to manually force an update, you can do so with $forceUpdate.

Cheap Static Components with v-once
Rendering plain HTML elements is very fast in Vue, but sometimes you might have a component that contains a lot of static content. In these cases, you can ensure that it’s only evaluated once and then cached by adding the v-once directive to the root element, like this:

Vue.component('terms-of-service', {
  template: `
    <div v-once>
      <h1>Terms of Service</h1>
      ... a lot of static content ...
    </div>
  `
})
Once again, try not to overuse this pattern. While convenient in those rare cases when you have to render a lot of static content, it’s simply not necessary unless you actually notice slow rendering – plus, it could cause a lot of confusion later. For example, imagine another developer who’s not familiar with v-once or simply misses it in the template. They might spend hours trying to figure out why the template isn’t updating correctly.